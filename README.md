# Contractor Tax Calculator

The project is developed using Java on Fedora 30. The purpose of the application is to help Contractors in Ireland to calculate their annual tax due to Revenue. It would be a useful for sole traders working as contractors.